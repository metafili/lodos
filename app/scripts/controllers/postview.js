'use strict';

app.controller('PostViewCtrl', function ($scope, $routeParams, Post, Auth, Firebase, $document) {
  $scope.post = Post.get($routeParams.postId);
  $scope.comments = Post.comments($routeParams.postId);
  $scope.flows = Post.flows($routeParams.postId);

  $scope.user = Auth.user;
  $scope.signedIn = Auth.signedIn;

  $scope.addComment = function () {
    if(!$scope.commentText || $scope.commentText === '') {
      return;
    }

    var comment = {
      text: $scope.commentText,
      body: $scope.commentBody,
      creator: $scope.user.profile.username,
      creatorUID: $scope.user.uid,
      timestamp: Firebase.ServerValue.TIMESTAMP
    };
    $scope.comments.$add(comment);

    $scope.commentText = '';
    $scope.commentBody = '';

  };
  
  $scope.deleteComment = function (comment) {
    $scope.comments.$remove(comment);
  };
 $scope.deleteFlow = function (flow) {
    $scope.flows.$remove(flow);
  };

    $scope.addFlow = function () {
    if(!$scope.flowText || $scope.flowText === '') {
      return;
    }

    var flow = {
      text: $scope.flowText,
      creator: $scope.user.profile.username,
      creatorUID: $scope.user.uid,
      timestamp: Firebase.ServerValue.TIMESTAMP
    };
    $scope.flows.$add(flow);
    Post.update($routeParams.postId, $scope.flowText);
    
    $scope.flowText = '';
  };


    $scope.mouseDown = function (event) {
            $scope.X = event.offsetX;
  $scope.width = document.getElementById('foo').offsetWidth;
$scope.prog = 100*$scope.X / $scope.width;
Post.update($routeParams.postId, $scope.prog);
             
        };
  

});
