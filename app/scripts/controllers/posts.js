'use strict';

  app.controller('PostsCtrl', function ($scope,$routeParams, $location, Post, Auth, $document) {
  $scope.posts = Post.all;
  $scope.comments = Post.comments.all;

  $scope.user = Auth.user;


  $scope.deletePost = function (post) {
    Post.delete(post);
  };

$scope.submitPost = function () {
    $scope.post.creator = $scope.user.profile.username;
    $scope.post.creatorUID = $scope.user.uid;
    Post.create($scope.post).then(function (ref) {
//      $location.path('/posts/' + ref.name());
      $scope.post = {url: 'http://', title: '', progress: ''};
    });
  };


  $scope.signedIn = Auth.signedIn;
  $scope.logout = Auth.logout;

});
