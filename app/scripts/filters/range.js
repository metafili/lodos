'use strict';

app.filter('progressRange', function () {
  return function (input) {
    
   if(input < 34) 
      return  "danger";
    else if (input < 67)
      return "warning";
    else if(input < 99)
      return  "info";
    else if (input < 101)
      return "success"
    else  
      return "danger";
  };
});